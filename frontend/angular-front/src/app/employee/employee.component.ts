import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  constructor(private _http: HttpClient) { }

  departments:any = [];
  employees:any = [];

  modalTitle = '';
  employeeId = 0;
  employeeName = '';
  department = '';
  dateOfJoining = '';
  photoFileName = 'Cover.jpeg';
  photoPath = environment.PHOTO_URL;

  ngOnInit(): void {
    this.refreshList();
  }

  refreshList() {
    this._http.get<any>(environment.API_URL + 'employee')
      .subscribe(data => {
        this.employees = data;
      });

    this._http.get<any>(environment.API_URL + 'department')
      .subscribe(data => {
        this.departments = data;
      });
  }

  addClick() {
    this.modalTitle = 'Add Employee';
    this.employeeId = 0;
    this.employeeName = '';
    this.department = '';
    this.dateOfJoining = '';
    this.photoFileName = "Cover.jpeg"
  }

  editClick(emp: any) {
    this.modalTitle = 'Add Employee';
    this.employeeId = emp.employeeId;
    this.employeeName = emp.employeeName;
    this.department = emp.department;
    this.dateOfJoining = emp.dateOfJoining;
    this.photoFileName = emp.photoFileName;
  }

  createClick() {
    let val = {
      employeeId: this.employeeId,
      employeeName: this.employeeName,
      department: this.department,
      dateOfJoining: this.dateOfJoining,
      photoFileName: this.photoFileName
    };

    this._http.post(environment.API_URL + 'employee', val)
      .subscribe(res => {
        alert(res.toString());
        this.refreshList();
      })
  }

  updateClick() {
    let val = {
      employeeId: this.employeeId,
      employeeName: this.employeeName,
      department: this.department,
      dateOfJoining: this.dateOfJoining,
      photoFileName: this.photoFileName
    };

    this._http.put(environment.API_URL + 'employee', val)
      .subscribe(res => {

        this.refreshList();
      })
  }

  deleteClick(id:any) {
    if (confirm('Are you sure?')) {
      this._http.delete(environment.API_URL + 'employee/' + id)
        .subscribe(res => {

          this.refreshList();
        });
    }
  }

  imageUpload(event:any) {
    let file = event.target.files[0];
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    this._http.post(environment.API_URL + 'employee/savefile', formData)
      .subscribe((data:any) => {
        this.photoFileName = data.toString();
      });
  }
}
