import { Component, OnInit } from '@angular/core';

import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss']
})
export class DepartmentComponent implements OnInit {

  constructor(private _http: HttpClient) { }

  departments: any = [];

  modalTitle = '';
  departmentId = 0;
  departmentName = '';

  departmentIdFilter = '';
  departmentNameFilter = '';
  departmentsWithoutFilter:any = [];

  sortingFlag = false;

  ngOnInit(): void {
    this.refreshList();
  }

  refreshList() {
    this._http.get<any>(environment.API_URL + 'department')
      .subscribe(data => {
        this.departments = data;
        this.departmentsWithoutFilter = data;


      });
  }

  addClick() {
    this.modalTitle = 'Add Department';
    this.departmentId = 0;
    this.departmentName = '';
  }

  editClick(dep: any) {
    this.modalTitle = 'Edit Department';
    this.departmentId = dep.departmentId;
    this.departmentName = dep.departmentName;
  }

  createClick() {
    let val = {
      departmentId: this.departmentId,
      departmentName: this.departmentName
    };

    this._http.post(environment.API_URL + 'department', val)
      .subscribe(res => {
        alert(res.toString());
        this.refreshList();
      })
  }

  updateClick() {
    let val = {
      departmentId: this.departmentId,
      departmentName: this.departmentName
    };

    this._http.put(environment.API_URL + 'department', val)
      .subscribe(res => {

        this.refreshList();
      })
  }

  deleteClick(id:any) {
    if (confirm('Are you sure?')) {
      this._http.delete(environment.API_URL + 'department/' + id)
        .subscribe(res => {

          this.refreshList();
        });
    }
  }

  filterResults() {
    let departmentIdFilter = this.departmentIdFilter;
    let departmentNameFilter = this.departmentNameFilter;

    this.departments = this.departmentsWithoutFilter.filter((el: any) => {
      let departmentIdFilterProved = departmentIdFilter.toString().trim().toLowerCase();
      let departmentNameFilterProved = departmentNameFilter.toString().trim().toLowerCase();

      return el.departmentId.toString().toLowerCase().includes(departmentIdFilterProved) &&
        el.departmentName.toString().toLowerCase().includes(departmentNameFilterProved)
      }
    );
  }

  sortResults(prop: string, asc: boolean) {
    this.sortingFlag = !asc;
    this.departments = this.departmentsWithoutFilter.sort((a: any, b: any) => {
      if (asc) {
        return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
      } else {
        return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
      }
    });
  }

}
