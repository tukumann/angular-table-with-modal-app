let express = require('express');
let bodyParser = require('body-parser');
const { MongoClient } = require("mongodb");

let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

const CONNECTION_STRING = 'mongodb+srv://timriene:iriene23@maincluster.f5qdq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

let fileUpload = require('express-fileupload');
let fs = require('fs');
let cors = require('cors');
app.use(cors());

// This code does not work on Mac due to read-only enhanced operating system security
/*const folderName = '/Photos';
try {
    if (!fs.existsSync(folderName)) {
        fs.mkdirSync(folderName)
    }
} catch (err) {
    console.error('probably does not work on Mac due to security', err)
} */

app.use(fileUpload());
app.use('/Photos', express.static(__dirname + '/Photos'));

const DATABASE = 'MainDB';
let database;

app.listen(49146, () => {
    MongoClient.connect(CONNECTION_STRING, {useNewUrlParser: true}, (error, client) => {
        database = client.db(DATABASE);
        console.log("MongoDB Connection Succesful");
    })
});

app.get('/', (req, res) => {
    res.json('An application works');
});

// Department Part

app.get('/api/department', (req, res) => {
    database.collection('Department').find({}).toArray((error, result) => {
        if (error) {
            console.log(error);
        }

        res.send(result.map((dep) => ({id: dep._id, departmentId: dep.departmentId, departmentName: dep.departmentName})));
    })
})

app.post('/api/department', (req, res) => {

    database.collection('Department').count({}, (error, numOfDocs) => {
        if (error) {
            console.log(error);
        }

        database.collection('Department').insertOne({
            departmentId: numOfDocs + 1,
            departmentName: req.body['departmentName']
        });

        res.json('Added successfully!');
    })
})

app.put('/api/department', (req, res) => {

        database.collection('Department').updateOne(
            // Filter Criteria
            {
                "departmentId": req.body['departmentId']
            },
            // Update
            {$set:
                {
                    "departmentName": req.body['departmentName']
                }
            }
        );

        res.json('Updated successfully!');
})

app.delete('/api/department/:id', (req, res) => {

    database.collection('Department').deleteOne(
        {
            departmentId: parseInt(req.params.id)
        }
    );

    res.json('Deleted successfully');
})

// Employee Part

app.get('/api/employee', (req, res) => {
    database.collection('Employee').find({}).toArray((error, result) => {
        if (error) {
            console.log(error);
        }

        res.send(result.map((emp) => ({id: emp._id, employeeId: emp.employeeId, employeeName: emp.employeeName, department: emp.department, dateOfJoining: emp.dateOfJoining, photoFileName: emp.photoFileName})));
    })
})

app.post('/api/employee', (req, res) => {

    database.collection('Employee').count({}, (error, numOfDocs) => {
        if (error) {
            console.log(error);
        }

        database.collection('Employee').insertOne({
            employeeId: numOfDocs + 1,
            employeeName: req.body['employeeName'],
            department: req.body['department'],
            dateOfJoining: req.body['dateOfJoining'],
            photoFileName: req.body['photoFileName']
        });

        res.json('Added successfully');
    })
})

app.put('/api/employee', (req, res) => {

    database.collection('Employee').updateOne(
        // Filter Criteria
        {
            "employeeId": req.body['employeeId']
        },
        // Update
        {$set:
                {
                    "employeeName": req.body['employeeName'],
                    "department": req.body['department'],
                    "dateOfJoining": req.body['dateOfJoining'],
                    "photoFileName": req.body['photoFileName']
                }
        }
    );

    res.json('Updated successfully!');
})

app.delete('/api/employee/:id', (req, res) => {

    database.collection("Employee").deleteOne(
        {
            employeeId: parseInt(req.params.id)
        }
    );

    res.json('Deleted successfully');
})

app.post('/api/employee/savefile', (req, res) => {
    fs.writeFile("./Photos/" + req.files.file.name,
    req.files.file.data, (err) => {
        if (err) throw err;
        res.json(req.files.file.name)
        }
    );
})